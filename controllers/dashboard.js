const Account = require("../models/Account")

exports.getDashboard = (req, res) => {
    const user = Account.findOne({user : req.username});
    res.render("manage/dashboard", {user:user});
}