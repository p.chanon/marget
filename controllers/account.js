const { render } = require("ejs");
const bcrypt = require("bcrypt");
const Account = require("../models/Account");
const { use } = require("../routes");
const jwt = require("jsonwebtoken");
const { append } = require("express/lib/response");
const axios = require("axios");
require("dotenv").config();

exports.getReg = (req, res) => {
  res.render("account/register");
};

exports.getLogin = (req, res) => {
  res.render("account/login");
};

const logout = () => {
  axios.get("/logout").then((res) => {
    res.redirect("/");
  });
};

exports.postLogin = async (req, res) => {
  const user = await Account.findOne({ username: req.body.username });

  if (!user) {
    return res.status(400).send("Cannot find user");
  }
  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      console.log("Login Success");
      //:: Create Token
      const token = jwt.sign({ user_id: user._id }, process.env.TOKEN_KEY, {
        expiresIn: "1d",
      });
      //:: Save Token
      res.cookie("token", token, {
        httpOnly: true,
        secure: true,
        maxAge: 86400000,
      });
      console.log(user);

      res.redirect("/");
    } else {
      res.send("Not Allowed");
    }
  } catch {
    res.status(500).send("Fail 500");
  }
};

const generatePassword = async (password) => {
  const passwordHashed = await bcrypt.hash(password, 10);
  return passwordHashed;
};

exports.postReg = async (req, res) => {
  const newuser = new Account({
    username: req.body.username,
    password: await generatePassword(req.body.password),
    email: req.body.email,
    phone: req.body.phone,
    fullname: req.body.fullname,
    lastname: req.body.lastname,
  });

  try {
    const saveuser = await newuser.save();
    res.render("index", { user: saveuser });
  } catch (error) {
    res.send(error);
  }
};
