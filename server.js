const express = require("express");
const mongoose = require("mongoose");
const app = express();
const routesIndex = require("./routes/index");
const routesAccount = require("./routes/account");
const routesShops = require("./routes/shops");
const routesDashboard = require("./routes/dashboard");
const bodyParser = require("body-parser");
const path = require("path");
const cookieParser = require("cookie-parser");
const auth = require("./middleware/auth");
const { addAbortSignal } = require("stream");
const Account = require("./models/Account");
require("dotenv").config();

app.set("view engine", "ejs");
app.set("views", "views");

app.use(express.json()); 
app.use(bodyParser.urlencoded({
    extended: true
}))


mongoose.connect(process.env.MONGOOSE_URI)
    .then(() => console.log(":: Connected to Database"))
    .catch((error) => console.log(":: Cannot Connect to Database"))

// exports.connect = () => {
//     mongoose.connect(process.env.MONGOOSE_URI, {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     }).then(() => {
//         console.log(":: Connected to Database");
//     }).catch((error) => {
//         console.log(":: Cannot connect to Database");
//     })
// }


app.use(routesIndex);
app.use(routesAccount);
app.use(routesShops);
app.use(routesDashboard);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/logout", (req, res) => {
    res.clearCookie("token");
    res.redirect("/");
});


app.listen(process.env.SERVER_PORT, () => {
    console.log(":: Server Started")
});


