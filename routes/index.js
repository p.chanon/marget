const express = require("express")
const router = express.Router();
const controllers = require("../controllers/index");
const Account = require("../models/Account");
const { findOne } = require("../models/Account");

router.get("/", controllers.getIndex);


module.exports = router;