const express = require("express")
const router = express.Router();
const controllers = require("../controllers/shops.js");

router.get("/shops", controllers.getShops);


module.exports = router;