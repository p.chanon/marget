const express = require("express")
const router = express.Router();
const controllers = require("../controllers/account");

router.get("/register", controllers.getReg);
router.post("/register", controllers.postReg);

router.get("/login", controllers.getLogin);
router.post("/login", controllers.postLogin);

module.exports = router;