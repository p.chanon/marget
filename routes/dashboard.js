const express = require("express")
const router = express.Router();
const controllers = require("../controllers/dashboard.js");
const verifyToken = require("../middleware/auth");

router.get("/dashboard", verifyToken, controllers.getDashboard);


module.exports = router;