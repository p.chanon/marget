import React from "react";
import { Link } from "react-router-dom";
import "./Home.css";
import Navbar from "../../components/Navbar";

const Home = () => {
  return (
    <div>
    <Navbar />
      <div class="home-wrapper">
        <span class="welcome-text">FREE SPACE FOR PROMOTE YOUR SHOP</span>
        <div class="grid-container">

        </div>
        <a href="/shops" class="more-shop-btn">
          MORE SHOPS
        </a>
      </div>
    </div>
  );
};

export default Home;
