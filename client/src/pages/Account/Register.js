import React from "react";
import { Link } from 'react-router-dom';
import "./Register.css";
import Navbar from "../../components/Navbar";

const Register = () => {
  return (
    <div>
      <Navbar />
      <div class="register-wrapper">
        <div class="title">
          <span>Logo marget</span>
        </div>
        <form action="/register" method="post">
          <div class="row">
            <i class="fas fa-user"></i>
            <input
              type="text"
              placeholder="Username"
              id="username"
              name="username"
            />
          </div>
          <div class="row">
            <i class="fas fa-lock"></i>
            <input
              type="password"
              placeholder="Password"
              id="password"
              name="password"
            />
          </div>
          <div class="row">
            <i class="fas fa-lock"></i>
            <input
              type="password"
              placeholder="Confirm Password"
              id="password2"
              name="password2"
            />
          </div>
          <div class="row">
            <i class="fas fa-at"></i>
            <input type="text" placeholder="Email" id="email" name="email" />
          </div>
          <div class="row">
            <i class="fas fa-phone-alt"></i>
            <input
              type="text"
              placeholder="Phone Number"
              id="phone"
              name="phone"
            />
          </div>
          <div class="row">
            <i class="fas fa-phone-alt"></i>
            <input
              type="text"
              placeholder="Full Name"
              id="fullname"
              name="fullname"
            />
          </div>
          <div class="row">
            <i class="fas fa-phone-alt"></i>
            <input
              type="text"
              placeholder="Last Name"
              id="lastname"
              name="lastname"
            />
          </div>
          <div class="row button">
            <input type="submit" value="Register" />
            <p>
              Already have an account ? <a href="/login">Sign in</a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
