import React from 'react'
import { Link } from 'react-router-dom'
import "./Login.css"
import Navbar from "../../components/Navbar";

const Login = () => {
  return (
    <div>
      <Navbar />
        <form action="/login" method="post">
            <label for="username">Username: </label>
            <input type="text" id="username" name="username"/>
            <label for="password">Password: </label>
            <input type="password" id="password" name="password"/>
            <input type="submit" value="Submit"/>
        </form>
    </div>
  )
}

export default Login