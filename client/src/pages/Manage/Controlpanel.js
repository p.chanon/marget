import React from 'react'
import { Link } from 'react-router-dom'
import "./Dashboard.css"
import Navbar from "../../components/Navbar";


const Controlpanel = () => {
  return (
    <div>
      <Navbar />
        <div class="dashboard-wrapper">
        <div class="btn-wrapper">
          <span class="dashboard-title">Control Panel</span>
          {/* <a href="#" class="add-shop">
            ADD NEW SHOP
          </a> */}
        </div>

        <div class="status-container">
          <table>
            <thead>
              <th>No</th>
              <th>Shop Title</th>
              <th>Shop Description</th>
              <th>Status</th>
              <th>Manage</th>
            </thead>
            <tr>
              <td>1</td>
              <td>Jaraphat Shop</td>
              <td>Lorem ipsum, dolor sit amet consectetur</td>
              <td>Approved</td>
              <td>
                <a href="#">
                  <i class="fa-solid fa-trash-can"></i>
                </a>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Jaraphat Shop</td>
              <td>Lorem ipsum, dolor sit amet consectetur</td>
              <td>Approved</td>
              <td>
                <a href="#">
                  <i class="fa-solid fa-trash-can"></i>
                </a>
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>Jaraphat Shop</td>
              <td>Lorem ipsum, dolor sit amet consectetur</td>
              <td>Decline</td>
              <td>
                <a href="#">
                  <i class="fa-solid fa-trash-can"></i>
                </a>
              </td>
            </tr>
            <tr>
              <td>4</td>
              <td>Jaraphat Shop</td>
              <td>Lorem ipsum, dolor sit amet consectetur</td>
              <td>Approved</td>
              <td>
                <a href="#">
                  <i class="fa-solid fa-trash-can"></i>
                </a>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  )
}

export default Controlpanel