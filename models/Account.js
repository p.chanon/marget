const mongoose = require("mongoose");
const AccountSchema = new mongoose.Schema({
    username: {type:String, required:true, unique:true},
    password: {type:String, required:true},
    email: {type:String, required:true, unique:true},
    phone: {type:String, required:true, unique:true},
    fullname: {type:String, required:true},
    lastname: {type:String, required:true},
    isAdmin: {type:Boolean, default:false},
});

module.exports = mongoose.model("Account", AccountSchema);